from datetime import datetime
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from .models import Post

# Create your views here.
def homepage(request):
	posts = Post.objects.all()
	post_lists = list()
	for count, post in enumerate(posts):
		post_lists.append("No.{}:".format(str(count)) + str(post.title))
	now = datetime.now()
	return render(request, 'index.html', locals())
	#t = get_template('index.html')
	#html = t.render(request, locals())
	#return HttpResponse(html)
	
	#post_lists = list()
	#for count, post in enumerate(posts):
	#	post_lists.append("No.{}:".format(str(count)) + str(post.title) + "<hr>")
	#	post_lists.append("<small>" + str(post.body) + "</small><br><br>")
	#return HttpResponse(post_lists)
	
def showpost(request, slug):
	try:
		post = Post.objects.get(slug = slug)
		if post != None:
			return render(request, 'post.html', locals())
	except:
		return redirect('/')